package com.javarush.test.level13.lesson11.bonus03;

public abstract class AbstractRobot implements Attackable, Defensable
{
    private static int hitCount;
    public BodyPart attack()
    {
        BodyPart attackedBodyPart = null;

        if (hitCount == 0) {
            attackedBodyPart =  BodyPart.ARM;
        } else if (hitCount == 1) {
            attackedBodyPart =  BodyPart.HEAD;
        } else if (hitCount == 2) {
            attackedBodyPart = BodyPart.CHEST;
        } else if (hitCount == 3) {
            hitCount = 0;
            attackedBodyPart =  BodyPart.LEG;
        }
        hitCount = hitCount + 1;
        return attackedBodyPart;
    }

    public BodyPart defense()
    {
        BodyPart defencedBodyPart = null;

        if (hitCount == 0) {
            defencedBodyPart =  BodyPart.HEAD;
        } else if (hitCount == 1) {
            defencedBodyPart =  BodyPart.LEG;
        } else if (hitCount == 2){
            defencedBodyPart = BodyPart.CHEST;
        } else if (hitCount == 3) {
            hitCount = 0;
            defencedBodyPart =  BodyPart.ARM;
        }
        hitCount = hitCount + 1;
        return defencedBodyPart;
    }
}