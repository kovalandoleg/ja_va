package com.javarush.test.level13.lesson11.bonus01;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

/** Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        InputStream inFile = null;
        ArrayList<Integer> parsedList = new ArrayList<Integer>();

        while (inFile == null)
        {
            try
            {
                inFile = new FileInputStream(reader.readLine());
            }
            catch (IOException e)
            {
                System.out.println("Incorect file name! Please try again");
            }
        }

        reader = new BufferedReader(new InputStreamReader(inFile));
        String tmpLine = null;
        int lineNumber = 0;
        while ((tmpLine = reader.readLine()) != null)
        {
            try
            {
                lineNumber++;
                if (tmpLine.equals(""))
                    continue;
                parsedList.add(Integer.parseInt(tmpLine));
            }
            catch (NumberFormatException e)
            {
                System.out.println("Wrong format in file at line №"+ lineNumber);
            }
        }
        inFile.close();
        reader.close();
        Collections.sort(parsedList);
        for (Integer x : parsedList)
        {
            if (x%2 == 0)
                System.out.println(x);
        }
    }
}