package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
/*    public static void main(String[] args)
    {
        HashMap<String,String> mymap = createMap();
        int fami = getCountTheSameLastName(mymap, "Familia5");
        int name = getCountTheSameFirstName(mymap,"Name3");
        System.out.println(fami);
        System.out.println(name);
    }*/
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> tmpHM = new HashMap<String, String>();
        for (int i = 0; i < 10; i++)
        {
            tmpHM.put("Familia"+i, "Name"+i);
        }
        return tmpHM;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        int cunt=0;
        for (Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator(); iter.hasNext();)
        {
            Map.Entry<String, String> entry = iter.next();
            if (entry.getValue().equals(name))
                cunt++;
        }
        return cunt;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
        int cunt = 0;
        for (Iterator<Map.Entry<String,String>> iter = map.entrySet().iterator(); iter.hasNext();)
        {
            Map.Entry<String, String> entry = iter.next();
            if (entry.getKey().equals(familiya))
                cunt++;
        }
        return cunt;
    }
}
