package com.javarush.test.level08.lesson08.task04;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
//    public static void main(String[] args)
//    {
//        HashMap<String,Date> myMap = createMap();
//        removeAllSummerPeople(myMap);
//        for (Map.Entry<String,Date> pair : myMap.entrySet())
//        {
//            System.out.println(pair.getKey() +" : "+ pair.getValue());
//        }
//    }


    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stal", new Date("JUNE 1 1980"));
        map.put("Vasa", new Date("JULY 1 1980"));
        map.put("Kola", new Date("AUGUST 1 1980"));
        map.put("Masa", new Date("MARCH 1 1980"));
        map.put("Dick", new Date("DECEMBER 1 1980"));
        map.put("Cunt", new Date("JANUARY 1 1980"));
        map.put("Fuck", new Date("APRIL 1 1980"));
        map.put("Faty", new Date("MAY 1 1980"));
        map.put("Sexy", new Date("JULY 1 1980"));
        map.put("Wood", new Date("AUGUST 1 1980"));

        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        for (Iterator<Map.Entry<String,Date>> iterat = map.entrySet().iterator(); iterat.hasNext();)
        {
            Map.Entry<String,Date> mapEntry = iterat.next();
            int tmp = mapEntry.getValue().getMonth();
            if (tmp == 5 || tmp == 6 || tmp == 7)
                iterat.remove();
        }
    }
}
