package com.javarush.test.level08.lesson11.home09;

import java.util.Date;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true,
если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Не забудьте учесть первый день года.
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args)
    {
//        System.out.println("MAY 1 2013 = " + isDateOdd("MAY 1 2013"));
//        System.out.println("JANUARY 1 2000 = " + isDateOdd("JANUARY 1 2000"));
    }

    public static boolean isDateOdd(String dateS)
    {
        Date indate = new Date(dateS);
        Date dubdate = new Date(dateS);
        dubdate.setMonth(0);
        dubdate.setDate(1);
        dubdate.setHours(0);
        dubdate.setMinutes(0);
        dubdate.setSeconds(0);
        int days = (int) ((indate.getTime() - dubdate.getTime())/(24*60*60*1000)) + 1;
//        Date indate = new Date(dateS);
//        Date comparedate = new Date(indate.getYear(), 0, 0);
//        int days =(int) (indate.getTime() - comparedate.getTime())/(24*60*60*1000);
//
        return (days % 2 != 0);
    }
}

//long lDate = dDate.getTime();
//lDate=lDate/1000/60/60/24%365;