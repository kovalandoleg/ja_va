package com.javarush.test.level08.lesson11.home01;

import com.javarush.test.level06.lesson05.task04.Cat;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* Set из котов
1. Внутри класса MySolution создать public static класс кот – Cat.
2. Реализовать метод createCats, он должен создавать множество (Set) котов и добавлять в него 3 кота.
3. В методе main удалите одного кота из Set cats.
4. Реализовать метод printCats, он должен вывести на экран всех котов, которые остались во множестве.
Каждый кот с новой строки.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Set<Cat> cats = createCats();

        //напишите тут ваш код. пункт 3
        Iterator<Cat> icat = cats.iterator();
        icat.next();
        icat.remove();
        printCats(cats);
    }

    public static Set<Cat> createCats()
    {
        //напишите тут ваш код. пункт 2
        Set<Cat> catSet = new HashSet<Cat>();
        catSet.add(new Cat());
        catSet.add(new Cat());
        catSet.add(new Cat());
        return catSet;
    }

    public static void printCats(Set<Cat> cats)
    {
        // пункт 4
        for (Cat cat : cats)
        {
            System.out.println(cat);
        }
    }

    // пункт 1
    public static class Cat
    {
    }
}
/*

public class MySolution
{
    public static void main(String[] args)
    {
        Set<Cat> cats = createCats();

        //напишите тут ваш код. пункт 3
        cats.remove(new Cat("Fuck"));
        printCats(cats);
    }

    public static Set<Cat> createCats()
    {
        //напишите тут ваш код. пункт 2
        Set<Cat> catSet = new HashSet<Cat>();
        catSet.add(new Cat("Cunt"));
        catSet.add(new Cat("Dick"));
        catSet.add(new Cat("Fuck"));
        return catSet;
    }

    public static void printCats(Set<Cat> cats)
    {
        // пункт 4
        for (Cat cat : cats)
        {
            System.out.println(cat);
        }
    }

    // пункт 1
    public static class Cat
    {
        public String name;

        public Cat(String name)
        {
            this.name = name;
        }

        @Override
        public int hashCode()
        {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj instanceof Cat) {
                Cat pp = (Cat) obj;
                return (pp.name.equals(this.name));
            } else {
                return false;
            }
        }
    }
}*/
