package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код :2,3
        Human grandpa1 = new Human("GrandPa1", true, 99);
        Human grandpa2 = new Human("GrandPa2", true, 88);
        Human grandma1 = new Human("GrandMa1", false, 101);
        Human grandma2 = new Human("GrandMa2", false, 28);
        Human father = new Human("Father", true, 50);
        Human mother = new Human("Mother", false, 66);
        Human child1 = new Human("Dick", true, 21);
        Human child2 = new Human("Dallas", false, 26);
        Human child3 = new Human("Cunt", false, 18);

        // setting children:
        grandpa1.setChild(father);
        grandma1.setChild(father);
        grandpa2.setChild(mother);
        grandma2.setChild(mother);
        father.setChild(child1);
        father.setChild(child2);
        father.setChild(child3);
        mother.setChild(child1);
        mother.setChild(child2);
        mother.setChild(child3);

        // Output:
        System.out.println(grandpa1);
        System.out.println(grandma1);
        System.out.println(grandpa2);
        System.out.println(grandma2);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(child1);
        System.out.println(child2);
        System.out.println(child3);
    }

    public static class Human
    {
        //напишите тут ваш код
        public String name;
        public boolean sex;
        public int age;
        public ArrayList<Human> children = new ArrayList<Human>();

        public Human(String name, boolean sex, int age)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
//            this.children.addAll(children); --- if we use ArrayList in parameters
        }

        public void setChild(Human child){ this.children.add(child); }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
