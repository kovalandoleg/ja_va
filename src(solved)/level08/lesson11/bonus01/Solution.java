package com.javarush.test.level08.lesson11.bonus01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
Используйте коллекции.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String instring = reader.readLine().toLowerCase();
        int count = 0;

        ArrayList<String> monthsList = new ArrayList<String>();
        monthsList.add("January");
        monthsList.add("February");
        monthsList.add("March");
        monthsList.add("April");
        monthsList.add("May");
        monthsList.add("June");
        monthsList.add("July");
        monthsList.add("August");
        monthsList.add("September");
        monthsList.add("October");
        monthsList.add("November");
        monthsList.add("December");

        for (int i = 0; i < monthsList.size(); i++)
        {
            if (monthsList.get(i).toLowerCase().equals(instring))
            {
                System.out.println(monthsList.get(i) + " is " + (i + 1) + " month");
                count++;
            }
        }
        if (count == 0)
            System.out.println("!Wrong Input!");
    }

}
