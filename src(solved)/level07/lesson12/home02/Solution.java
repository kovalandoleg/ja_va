package com.javarush.test.level07.lesson12.home02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Переставить M первых строк в конец списка
Ввести с клавиатуры 2 числа N  и M.
Ввести N строк и заполнить ими список.
Переставить M первых строк в конец списка.
Вывести список на экран, каждое значение с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int N = Integer.parseInt(reader.readLine());
        int M = Integer.parseInt(reader.readLine());
        ArrayList<String> inList = new ArrayList<String>(N);
        String tmp = null;

        for (int i = 0; i < N; i++)
        {
            inList.add(reader.readLine());
        }

        for (int i = 0; i < M; i++)
        {
            tmp = inList.get(0);
            inList.remove(0);
            inList.add(tmp);
        }

        for (String out : inList)
        {
            System.out.println(out);
        }
    }
}
