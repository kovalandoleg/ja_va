package com.javarush.test.level18.lesson03.task01;

import java.io.*;

/** Максимальный байт
Ввести с консоли имя файла
Найти максимальный байт в файле, вывести его на экран.
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream inFile = new FileInputStream(reader.readLine());
        int max = Integer.MIN_VALUE;
        int tmp;
        while (inFile.available() > 0){
            tmp = inFile.read();
            if (tmp > max)
                max = tmp;
        }
        System.out.println(max);
        inFile.close();
    }
}

//////////////////////////////////////////////////////////////////////
//package com.javarush.test.level18.lesson03.task01;
//
//        import java.io.FileInputStream;
//
///* Максимальный байт
//Ввести с консоли имя файла
//Найти максимальный байт в файле, вывести его на экран.
//Закрыть поток ввода-вывода
//*/
//
//public class Solution {
//    public static void main(String[] args) throws Exception {
//    }
//}