package com.javarush.test.level09.lesson11.home08;

import java.util.ArrayList;

/* Список из массивов чисел
Создать список, элементами которого будут массивы чисел. Добавить в список пять объектов–массивов длиной 5, 2, 4, 7, 0 соответственно. Заполнить массивы любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<int[]> list = createList();
        printList(list);
    }

    public static ArrayList<int[]> createList()
    {
        ArrayList<int[]> result = new ArrayList<int[]>();
        int[] a = {5,5,5,5,5};
        result.add(a);
        int[] b = {2,2};
        result.add(b);
        int[] c = {4,4,4,4};
        result.add(c);
        int[] d = {7,7,7,7,7,7,7};
        result.add(d);
        int[] e = {};
        result.add(e);
        return result;
    }

    public static void printList(ArrayList<int[]> list)
    {
        for (int[] array: list )
        {
            for (int x: array)
            {
                System.out.println(x);
            }
        }
    }
}
