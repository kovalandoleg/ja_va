package com.javarush.test.level15.lesson12.home05;

/**
 * Created on 20.10.15.
 */
public class SubSolution extends Solution
{
    public SubSolution() {}

    public SubSolution(int i) { super(i); }

    public SubSolution(short s) { super(s); }

    protected SubSolution(char c) { super(c); }

    protected SubSolution(String s) { super(s); }

    protected SubSolution(float f) { super(f); }

    SubSolution(double d) { super(d); }

    SubSolution(long l) { super(l); }

    SubSolution(boolean b) { super(b); }

    private SubSolution(short[] as) { super(); }

    private SubSolution(char[] ac) { super(); }

    private SubSolution(boolean[] ab) { super(); }
}
