package com.javarush.test.level15.lesson12.home09;

import java.io.*;
import java.util.ArrayList;

/** Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14

 * or try javarush.ru/alpha/index.html?lvl=15&??view&&&name=Aobjmigo&obj=3.14&name=&obj=djsdcd&&?oobj=3.0
 * obj.javarush.ru/?op=obj&&obj=3&obj=3.3.3&values=5&object=this&obj=&oobj=7.7
 *
 * 1 После? все знаки? воспринимаются как разделители &
 2 Надо пробовать преобразовывать к Double все строки, даже если там нет "."

 Подсказки:
 1 Убрать двойные разделители
 2 Через try парсить число
 3 Использовать сплиты строк, чтобы получить массив строк. удобно обрабатывать параметры
 4 Не забывать про пустые имена параметров и значения
*/

public class Solution {
    public static void main(String[] args) throws IOException
    {
        ArrayList<String> paramList = new ArrayList<String>();          //list of all parametres
        int objCount = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean stop = false;
        String URL = br.readLine();                                //reading URL from console
        String URL_param = URL.substring(URL.indexOf("?")+1);     //creating URL_param from only parameters

        while (!stop) {
            int indexOf_AND, indexOf_EQUAL;
            String param;

            if (URL_param.contains("&")) stop = false;            //if we don't have & then STOP for next cycle
            else stop = true;                                     //else go

            indexOf_AND = URL_param.indexOf("&");                   //index of symbol &
            if (indexOf_AND == -1) indexOf_AND = URL_param.length();//if  index <0 then ok

            param = URL_param.substring(0, indexOf_AND);            //delete all KROME one param

            indexOf_EQUAL = param.indexOf("=");
            if (indexOf_EQUAL == -1) indexOf_EQUAL = param.length();

            param = param.substring(0, indexOf_EQUAL);              //only parametr

            paramList.add(param);
            if (param.equals("obj")) objCount++;
            URL_param = URL_param.substring(URL_param.indexOf("&")+1);
        }

        for (String s: paramList) {
            System.out.print(s + " ");
        }
        System.out.println();

        if (objCount!=0) {
            URL_param = URL.substring(URL.indexOf("?")+1);
            int indexOf_OBJ = URL_param.indexOf("obj");
            URL_param = URL_param.substring(indexOf_OBJ);
            for (int i=0; i<objCount; i++) {
                String param;
                int indexOfAND = URL_param.indexOf("&");
                if (indexOfAND == -1) indexOfAND = URL_param.length();
                param = URL_param.substring(URL_param.indexOf("=") + 1, indexOfAND);

                try {
                    alert(Double.parseDouble(param));
                } catch (Exception e) {
                    alert(param);
                }
                URL_param = URL_param.substring(URL_param.indexOf("&")+1);
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}