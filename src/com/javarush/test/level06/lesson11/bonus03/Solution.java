package com.javarush.test.level06.lesson11.bonus03;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/** Задача по алгоритмам
Задача: Написать программу, которая вводит с клавиатуры 5 чисел и выводит их в возрастающем порядке.
Пример ввода:
3
2
15
6
17
Пример вывода:
2
3
6
15
17
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader  = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());
        int e = Integer.parseInt(reader.readLine());
        int min1, min2, min3, min4, min5;

        min1 = a < b ? a : b;
        min1 = min1 < c ? min1 : c;
        min1 = min1 < d ? min1 : d;
        min1 = min1 < e ? min1 : e;
        if (min1 == a)
            a = Integer.MAX_VALUE;
        else if (min1 == b)
            b = Integer.MAX_VALUE;
        else if (min1 == c)
            c = Integer.MAX_VALUE;
        else if (min1 == d)
            d = Integer.MAX_VALUE;
        else if (min1 == e)
            e = Integer.MAX_VALUE;

        min2 = a < b ? a : b;
        min2 = min2 < c ? min2 : c;
        min2 = min2 < d ? min2 : d;
        min2 = min2 < e ? min2 : e;
        if (min2 == a)
            a = Integer.MAX_VALUE;
        else if (min2 == b)
            b = Integer.MAX_VALUE;
        else if (min2 == c)
            c = Integer.MAX_VALUE;
        else if (min2 == d)
            d = Integer.MAX_VALUE;
        else if (min2 == e)
            e = Integer.MAX_VALUE;

        min3 = a < b ? a : b;
        min3 = min3 < c ? min3 : c;
        min3 = min3 < d ? min3 : d;
        min3 = min3 < e ? min3 : e;
        if (min3 == a)
            a = Integer.MAX_VALUE;
        else if (min3 == b)
            b = Integer.MAX_VALUE;
        else if (min3 == c)
            c = Integer.MAX_VALUE;
        else if (min3 == d)
            d = Integer.MAX_VALUE;
        else if (min3 == e)
            e = Integer.MAX_VALUE;

        min4 = a < b ? a : b;
        min4 = min4 < c ? min4 : c;
        min4 = min4 < d ? min4 : d;
        min4 = min4 < e ? min4 : e;
        if (min4 == a)
            a = Integer.MAX_VALUE;
        else if (min4 == b)
            b = Integer.MAX_VALUE;
        else if (min4 == c)
            c = Integer.MAX_VALUE;
        else if (min4 == d)
            d = Integer.MAX_VALUE;
        else if (min4 == e)
            e = Integer.MAX_VALUE;

        min5 = a < b ? a : b;
        min5 = min5 < c ? min5 : c;
        min5 = min5 < d ? min5 : d;
        min5 = min5 < e ? min5 : e;
        if (min5 == a)
            a = Integer.MAX_VALUE;
        else if (min5 == b)
            b = Integer.MAX_VALUE;
        else if (min5 == c)
            c = Integer.MAX_VALUE;
        else if (min5 == d)
            d = Integer.MAX_VALUE;
        else if (min5 == e)
            e = Integer.MAX_VALUE;

        System.out.println(min1);
        System.out.println(min2);
        System.out.println(min3);
        System.out.println(min4);
        System.out.println(min5);
    }
}