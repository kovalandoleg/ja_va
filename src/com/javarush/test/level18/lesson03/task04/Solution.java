package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/** Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        FileInputStream fileStream = new FileInputStream(fileName);
        ArrayList<Integer> byteList = new ArrayList<>();

        while (fileStream.available() > 0) {
            byteList.add(fileStream.read());
        }
        fileStream.close();

        GetBytesWithMaxRepetitions(byteList);
    }

    public static void GetBytesWithMaxRepetitions(ArrayList<Integer> list)
    {
        HashMap<Integer, Integer> result = new HashMap<Integer, Integer>();

        int count;
        int minCount = Integer.MAX_VALUE;

        for (int i = 0; i < list.size(); i++)
        {
            Integer currentInt = list.get(i);
            if (result.containsKey(currentInt))
                continue;
            count = 1;
            for (int j = i+1; j < list.size(); j++)
            {
                if (currentInt.equals(list.get(j)))
                    count++;
            }
            minCount = count < minCount ? count : minCount;
            result.put(currentInt, count);
        }

        for (Map.Entry<Integer, Integer> e : result.entrySet()) {
            Integer key = e.getKey();
            Integer value = e.getValue();
            if (value == minCount)
                System.out.print(key +" ");
        }

    }
}