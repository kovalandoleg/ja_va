package com.javarush.test.level18.lesson05.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/** DownloadException
1 Считывать с консоли имена файлов.
2 Если файл меньше 1000 байт, то:
2.1 Закрыть потоки
2.2 выбросить исключение DownloadException
*/

public class Solution {
    public static void main(String[] args) throws DownloadException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inFile;

        try {
            while (true){
                inFile = new FileInputStream(reader.readLine());
                if (inFile.available() < 1000)
                    throw new DownloadException();
            }
        } catch (Exception e) {
            if (e.getClass().equals(new DownloadException().getClass()))
                throw new DownloadException();
        }
    }

    public static class DownloadException extends Exception{

    }
}
