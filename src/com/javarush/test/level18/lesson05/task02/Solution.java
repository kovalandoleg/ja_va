package com.javarush.test.level18.lesson05.task02;

/** Подсчет запятых
С консоли считать имя файла
Посчитать в файле количество символов ',', количество вывести на консоль
Закрыть потоки. Не использовать try-with-resources

Подсказка: нужно сравнивать с ascii-кодом символа ','
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inFile = new FileInputStream(reader.readLine());
        int counter = 0;

        while(inFile.available() > 0){
            if (inFile.read() == 44)
                counter++;
        }
        reader.close();
        inFile.close();

        System.out.println(counter);
    }
}
