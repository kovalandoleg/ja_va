package com.javarush.test.level18.lesson10.bonus01;

import java.io.*;

/** Шифровка
Придумать механизм шифровки/дешифровки

Программа запускается с одним из следующих наборов параметров:
-e fileName fileOutputName
-d fileName fileOutputName
где
fileName - имя файла, который необходимо зашифровать/расшифровать
fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования
-e - ключ указывает, что необходимо зашифровать данные
-d - ключ указывает, что необходимо расшифровать данные
*/

public class Solution {

    private final static int key = 31122012;

    public static void main(String[] args) {

        //args = new String[] {"-e", "/home/nook/TMP/1", "/home/nook/TMP/2"};
        //args = new String[] {"-d", "/home/nook/TMP/2", "/home/nook/TMP/3"};

        if (args.length < 3)
            return;

        switch (args[0]){
            case "-e": Encrypt(args[1], args[2]);
                break;
            case "-d": Decrypt(args[1], args[2]);
                break;
            default : System.out.println("Wrong parameters! use \"-e\" or \"-d\"");
                break;
        }
    }

    public static void Encrypt(String in, String out) {
        FileInputStream inFile = null;
        FileOutputStream outFile = null;

        try
        {
            inFile = new FileInputStream(in);
            outFile = new FileOutputStream(out);
        } catch (Exception e) {
            System.out.println("Error ocured while opening files");
            e.printStackTrace();
            return;
        }

        try
        {
            int buffer;

            while (inFile.available() > 0)
            {
                buffer = inFile.read();
                buffer ^= key;
                outFile.write(buffer);
            }

            inFile.close();
            outFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void Decrypt(String in, String out) {
        FileInputStream inFile = null;
        FileOutputStream outFile = null;

        try
        {
            inFile = new FileInputStream(in);
            outFile = new FileOutputStream(out);
        } catch (Exception e) {
            System.out.println("Error ocured while opening files");
            e.printStackTrace();
            return;
        }

        try
        {
            int buffer;

            while (inFile.available() > 0)
            {
                buffer = inFile.read();
                buffer ^= key;
                outFile.write(buffer);
            }

            inFile.close();
            outFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/**
 *
 * E(x,key)=> y= x XOR key
 * D(y,key)=> x= y XOR key
 *
 * E(b0,key)=> y0= b0 XOR key
 * E(b1,y0)=> y1= b0 XOR y0 .... E(bn,yn-1)=> yn= bn XOR yn-1
 *
 **/
