package com.javarush.test.level04.lesson06.task02;

/** Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String sa = reader.readLine();
        int a = Integer.parseInt(sa);
        String sb = reader.readLine();
        int b = Integer.parseInt(sb);
        String sc = reader.readLine();
        int c = Integer.parseInt(sc);
        String sd = reader.readLine();
        int d = Integer.parseInt(sd);

        if (a > b && a > c && a > d)
            System.out.print(a);
        else if (b > a && b > c && b > d)
            System.out.print(b);
        else if (c > a && c > b && c > d)
            System.out.print(c);
        else if (d > a && d > b && d > c)
            System.out.print(d);
    }
}
