package com.javarush.test.level13.lesson11.home03;

import java.io.*;

/** Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Не забыть освободить ресурсы. Закрыть поток чтения с файла и поток ввода с клавиатуры.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here
        FileInputStream infile = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true)
        {
            try
            {
                infile = new FileInputStream(reader.readLine());
                break;
            }
            catch (Exception e)
            {
                System.out.println("No such file! Try again.");
            }
        }

        reader = new BufferedReader(new InputStreamReader(infile));
        String out = null;

        while ((out = reader.readLine()) != null)
        {
            System.out.println(out);
        }
        infile.close();
        reader.close();
    }
}