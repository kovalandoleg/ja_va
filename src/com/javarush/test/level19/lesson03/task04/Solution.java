package com.javarush.test.level19.lesson03.task04;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/** И еще один адаптер
Адаптировать Scanner к PersonScanner.
Классом-адаптером является PersonScannerAdapter.
Данные в файле хранятся в следующем виде:
Иванов Иван Иванович 31 12 1950

В файле хранится большое количество людей, данные одного человека находятся в одной строке. Метод read() должен читать данные одного человека.
*/

public class Solution {
    public static class PersonScannerAdapter implements PersonScanner {
        private Scanner scanner;

        public PersonScannerAdapter(Scanner scanner) {
            this.scanner = scanner;
        }

        @Override
        public Person read() throws IOException
        {
            String line = scanner.nextLine();
            String[] splitLine = line.split(" ");

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyyyy");
            Date bd = null;
            try {
                bd = simpleDateFormat.parse(splitLine[3] + splitLine[4] + splitLine[5]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new Person(splitLine[1], splitLine[2], splitLine[0], bd);
        }

        @Override
        public void close() throws IOException
        {
            scanner.close();
        }
    }
}
