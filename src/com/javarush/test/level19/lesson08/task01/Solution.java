package com.javarush.test.level19.lesson08.task01;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/** Ридер обертка
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна преобразовывать весь текст в заглавные буквы
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток.
Вывести модифицированную строку в консоль.
*/

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        /** Saving original System.out */
        PrintStream consoleStream = System.out;
        /** Creating dynamic array */
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        /** Creating adapter for class Printstream */
        PrintStream streamForReplacement = new PrintStream(outputStream);
        /** Replacing System.out */
        System.setOut(streamForReplacement);

        testString.printSomething();

        String result = outputStream.toString().toUpperCase();

        System.setOut(consoleStream);

        System.out.println(result);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
        }
    }
}