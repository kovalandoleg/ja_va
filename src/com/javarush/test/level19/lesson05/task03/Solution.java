package com.javarush.test.level19.lesson05.task03;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Scanner;

/** Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String out = "";
        Scanner scanner = new Scanner(new File(reader.readLine()));
        FileWriter output = new FileWriter(reader.readLine());

        while (scanner.hasNext()){
            if (scanner.hasNextInt())
                out += scanner.nextInt() + " ";
            else
                scanner.next();
        }

        output.write(out);
        reader.close();
        scanner.close();
        output.close();
    }
}
