package com.javarush.test.level19.lesson05.task04;

import java.io.*;
import java.util.Scanner;

/** Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner = new Scanner(new File(reader.readLine())).useDelimiter("\\Z");
        FileWriter fileWriter = new FileWriter(new File(reader.readLine()));

        String content = scanner.next();

        fileWriter.write(content.replace(".", "!"));

        reader.close();
        scanner.close();
        fileWriter.close();
    }
}
