package com.javarush.test.level19.lesson05.task02;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static void main(String[] args) {

        String fileName;
        int count = 0;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            fileName = reader.readLine();
            reader.close();

            FileReader fileReader = new FileReader(fileName);

            Pattern pattern = Pattern.compile("\\bworld\\b", Pattern.CASE_INSENSITIVE);
            String str = "";

            while (fileReader.ready()) {
                str = str + (char) fileReader.read();
            }
            fileReader.close();

            Matcher matcher = pattern.matcher(str);

            while (matcher.find()) {
                count++;
            }

            System.out.println(count);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
