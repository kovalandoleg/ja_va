package com.javarush.test.level19.lesson05.task05;

import java.io.*;
import java.util.Scanner;

/** Пунктуация
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Удалить все знаки пунктуации, включая символы новой строки. Результат вывести во второй файл.
http://ru.wikipedia.org/wiki/%D0%9F%D1%83%D0%BD%D0%BA%D1%82%D1%83%D0%B0%D1%86%D0%B8%D1%8F
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner = new Scanner(new File(reader.readLine())).useDelimiter("\\Z");
        FileWriter fileWriter = new FileWriter(new File(reader.readLine()));

        String content = scanner.next();

        fileWriter.write(content.replaceAll("\\p{Punct}", ""));

        reader.close();
        scanner.close();
        fileWriter.close();
    }
}
