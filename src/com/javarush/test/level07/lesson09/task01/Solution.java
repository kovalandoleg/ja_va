package com.javarush.test.level07.lesson09.task01;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/** Три массива
1. Введи с клавиатуры 20 чисел, сохрани их в список и рассортируй по трём другим спискам:
Число делится на 3 (x%3==0), делится на 2 (x%2==0) и все остальные.
Числа, которые делятся на 3 и на 2 одновременно, например 6, попадают в оба списка.
2. Метод printList должен выводить на экран все элементы списка с новой строки.
3. Используя метод printList выведи эти три списка на экран. Сначала тот, который для x%3,
потом тот, который для x%2, потом последний.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<Integer> inList = new ArrayList<Integer>(20);
        ArrayList<Integer> x3List = new ArrayList<Integer>();
        ArrayList<Integer> x2List = new ArrayList<Integer>();
        ArrayList<Integer> xxList = new ArrayList<Integer>();

        for (int i = 0; i < 20; i++)
        {
            inList.add(i, Integer.parseInt(reader.readLine()));
            if (inList.get(i) % 3 == 0 && inList.get(i) % 2 == 0)
            {
                x3List.add(inList.get(i));
                x2List.add(inList.get(i));
            } else if (inList.get(i) % 3 == 0)
                x3List.add(inList.get(i));
            else if (inList.get(i) % 2 == 0)
                x2List.add(inList.get(i));
            else
                xxList.add(inList.get(i));
        }

        printList(x3List);
        printList(x2List);
        printList(xxList);
    }

    public static void printList(List<Integer> list)
    {
        for (Integer x : list)
            System.out.println(x);
    }
}
