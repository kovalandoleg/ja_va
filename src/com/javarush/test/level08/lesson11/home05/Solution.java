package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/** Мама Мыла Раму. Теперь с большой буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Вывести результат на экран.

Пример ввода:
  мама     мыла раму.

Пример вывода:
  Мама     Мыла Раму.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inS = reader.readLine(), outS = "";
        String[] splitedS = inS.split(" ");
        for (int i = 0; i < splitedS.length; i++)
        {
            char[] wordarray = splitedS[i].toCharArray();
            if (wordarray.length == 0)
                continue;
            wordarray[0] = Character.toUpperCase(wordarray[0]);
            splitedS[i] = new String(wordarray);
        }
        for (int i = 0; i < splitedS.length; i++)
        {
            outS = outS + splitedS[i] + " ";
        }
        System.out.println(outS);
    }
}
