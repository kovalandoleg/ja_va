package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Set;

/** Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        HashSet<Integer> tmpSet = new HashSet<Integer>();
        for (int i = 0; i < 20; i++)
        {
            tmpSet.add(i);
        }
        return tmpSet;
    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        HashSet<Integer> outHash = new HashSet<Integer>();
        for (Integer xint : set)
        {
            if (xint < 10)
                outHash.add(xint);
        }
        return outHash;
    }
}
