package com.javarush.test.level08.lesson08.task05;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/** Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static void main(String[] args)
    {
        HashMap<String,String> myMap = createMap();
        removeTheFirstNameDuplicates(myMap);
        for (Map.Entry<String,String> pair : myMap.entrySet())
        {
            System.out.println(pair.getKey() +" : "+ pair.getValue());
        }
    }

    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> tmpHM = new HashMap<String, String>();

        tmpHM.put("Пугачева", "Алла");
        tmpHM.put("Басков", "Алла");
        tmpHM.put("Джигурда", "Никита");
        tmpHM.put("Чехов", "Джордж");
        tmpHM.put("Павлиашвили", "Сосо");
        tmpHM.put("Абв", "Влад");
        tmpHM.put("Вгд", "Влад");
        tmpHM.put("Лепс", "Джордж");
        tmpHM.put("Д'Арк", "Жанна");
        tmpHM.put("Климов", "Джордж");

        return tmpHM;
    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        HashMap<String,String> copy = new HashMap<String, String>(map);

        for (Iterator<Map.Entry<String,String>> iterator = copy.entrySet().iterator(); iterator.hasNext();)
        {
            Map.Entry<String,String> entry = iterator.next();
            String name = entry.getValue();
            for (Iterator<Map.Entry<String,String>> iterator2 = copy.entrySet().iterator(); iterator2.hasNext();)
            {
                Map.Entry<String,String> entry2 = iterator2.next();
                if (entry2.getValue().equals(name) && !entry2.getKey().equals(entry.getKey()))
                    removeItemFromMapByValue(map, name);
            }
        }
    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}
