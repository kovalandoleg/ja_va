package com.javarush.test.level16.lesson13.bonus01;

import com.javarush.test.level16.lesson13.bonus01.common.*;

public class ImageReaderFactory
{
    public static ImageReader getReader(ImageTypes type)
    {
        ImageReader reader;
        try
        {
            if (type.equals(ImageTypes.BMP))
                reader = new BmpReader();
            else if (type.equals(ImageTypes.JPG))
                reader = new JpgReader();
            else if (type.equals(ImageTypes.PNG))
                reader = new PngReader();
            else
                throw new Exception();
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Неизвестный тип картинки");
        }
        return reader;
    }
}
