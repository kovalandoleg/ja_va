package com.javarush.test.level17.lesson10.bonus01;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/** CRUD
CrUD - Create, Update, Delete
Программа запускается с одним из следующих наборов параметров:
-c name sex bd
-u id name sex bd
-d id
-i id
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-c  - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
-u  - обновляет данные человека с данным id
-d  - производит логическое удаление человека с id
-i  - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)

id соответствует индексу в списке
Все люди должны храниться в allPeople
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat
Пример параметров: -c Миронов м 15/04/1990
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();
    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        //start here - начни тут
        if (args.length < 2)
            return;
        switch (args[0]) {
            case "-c" : CreateUser(args);
                break;
            case "-u" : UpdateUser(args);
                break;
            case "-d" : DeleteUser(args);
                break;
            case "-i" : ShowUser(args);
                break;
        }
    }

    public static void CreateUser(String[] args){
        Date bd = ParceDate(args[3]);
        if (args[2].equals("м"))
            allPeople.add(Person.createMale(args[1], bd));
        else
            allPeople.add(Person.createFemale(args[1], bd));
        System.out.println(allPeople.size()-1);
    }

    public static void UpdateUser(String[] args){
        Date bd = ParceDate(args[4]);
        Person per = allPeople.get(Integer.parseInt(args[1]));
        per.setBirthDay(bd);
        per.setName(args[2]);
        if (args[3].equals("м"))
            per.setSex(Sex.MALE);
        else
            per.setSex(Sex.FEMALE);
    }

    public static void DeleteUser(String[] args){
        allPeople.set(Integer.parseInt(args[1]), null);
    }

    public static void ShowUser(String[] args){
        Person person = allPeople.get(Integer.parseInt(args[1]));
        String sex = person.getSex() == Sex.MALE ? "м" : "ж";
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        System.out.println(person.getName() +" "+ sex +" "+ formatter.format(person.getBirthDay()));
    }

    public static Date ParceDate(String date_string){
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            Date date = sdf.parse(date_string);
            return date;
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            return new Date(0);
        }
    }
}
