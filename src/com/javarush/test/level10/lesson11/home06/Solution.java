package com.javarush.test.level10.lesson11.home06;

/** Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args) {}

    public static class Human
    {
        String name;
        String surname;
        String address;
        int age = Integer.valueOf(25);
        boolean sex;
        int height = Integer.valueOf(150);

        public Human(boolean sex)
        {
            this.sex = sex;
            this.address = "nowhere";
            this.name = "Hobo";
            this.surname = "Hobo";
        }

        public Human(String name,boolean sex)
        {
            this.name    = name;
            this.surname = "Unknow";
            this.address = "Unknow";
            this.sex     = sex;
        }

        public Human(String name, String surname, boolean sex)
        {
            this.name    = name;
            this.surname = surname;
            this.address = "Unknow";
            this.sex     = sex;
        }

        public Human(String name,String surname, boolean sex, String address)
        {
            this.name    = name;
            this.surname = surname;
            this.address = address;
            this.sex     = sex;
        }

        public Human(String name,String surname, boolean sex, String address, int age)
        {
            this.name    = name;
            this.surname = surname;
            this.address = address;
            this.sex     = sex;
            this.age     = age;
        }

        public Human(String name,String surname, boolean sex, String address, int age, int height)
        {
            this.name    = name;
            this.surname = surname;
            this.address = address;
            this.sex     = sex;
            this.age     = age;
            this.height  = height;
        }

        public Human(String name,String surname, boolean sex, int age)
        {
            this.name    = name;
            this.surname = surname;
            this.address = "nowhere";
            this.sex     = sex;
            this.age     = age;
        }

        public Human(String name,String surname, boolean sex, int age, int height)
        {
            this.name    = name;
            this.surname = surname;
            this.address = "nowhere";
            this.sex     = sex;
            this.age     = age;
            this.height  = height;
        }

        public Human(boolean sex, int age, int height)
        {
            this.name    = "unknow";
            this.surname = "unknow";
            this.address = "nowhere";
            this.sex     = sex;
            this.age     = age;
            this.height  = height;
        }

        public Human()
        {
            this.name    = "New";
            this.surname = "Born";
            this.address = "Hospital";
            this.sex     = true;
            this.age     = 0;
            this.height  = 30;
        }
    }
}
