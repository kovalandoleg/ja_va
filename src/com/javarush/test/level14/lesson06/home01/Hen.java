package com.javarush.test.level14.lesson06.home01;

/**
 * Created on 13.10.15.
 */
public abstract class Hen
{
    public abstract int getCountOfEggsPerMonth();

    public String getDescription()
    {
        return "Я курица.";
    }
}
