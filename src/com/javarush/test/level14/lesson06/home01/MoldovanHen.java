package com.javarush.test.level14.lesson06.home01;

/**
 * Created on 10/11/15.
 */
public class MoldovanHen extends Hen
{
    @Override
    public int getCountOfEggsPerMonth()
    {
        return 10000;
    }

    @Override
    public String getDescription()
    {
        return super.getDescription() +" Моя страна - "+ Country.MOLDOVA +". Я несу "+ this.getCountOfEggsPerMonth() +" яиц в месяц.";
    }
}
