package com.javarush.test.level14.lesson06.home01;

/**
 * Created on 10/11/15.
 */
public class UkrainianHen extends Hen
{
    @Override
    public int getCountOfEggsPerMonth()
    {
        return Integer.MAX_VALUE;
    }

    @Override
    public String getDescription()
    {
        return super.getDescription() +" Моя страна - "+ Country.UKRAINE +". Я несу "+ this.getCountOfEggsPerMonth() +" яиц в месяц.";
    }
}