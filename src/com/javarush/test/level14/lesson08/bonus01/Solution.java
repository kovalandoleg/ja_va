package com.javarush.test.level14.lesson08.bonus01;

import java.util.ArrayList;
import java.util.List;

/** Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //Add your code here
        //2-nd
        try
        {
            int[] i = new int[2];
            i[3] = 0;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //3-rd
        try
        {
            int x = Integer.parseInt("XXX");

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //4-th
        try
        {
            ArrayList<String> x = null;
            x.add("xxx");

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //5-th
        try
        {
            int[] x = new int[-1];

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //6-th
        try
        {
            throw new IllegalStateException();

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //7-th
        try
        {
            throw new SecurityException();

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //8-th
        try
        {
            throw new ClassNotFoundException();

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //9-th
        try
        {
            throw new CloneNotSupportedException();

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        //10-th
        try
        {
            throw new InterruptedException();

        } catch (Exception e)
        {
            exceptions.add(e);
        }
    }
}