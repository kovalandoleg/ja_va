package com.javarush.test.level14.lesson08.home01;

/**
 * Created on 13.10.15.
 */
interface Bridge
{
    int getCarsCount();
}
