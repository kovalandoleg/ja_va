package com.javarush.test.level14.lesson08.home09;

/**
 * Created on 15.10.15.
 */
public class USD extends Money
{
    public USD(double amount)
    {
        super(amount);
    }

    @Override
    public String getCurrencyName()
    {
        return "USD";
    }
}
