package com.javarush.test.level14.lesson08.bonus02;

/** НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int x = 0, y = 0;

        try
        {
            x = Integer.parseInt(reader.readLine());
            y = Integer.parseInt(reader.readLine());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println(returnNOD(x,y));
    }

    public static int returnNOD(int x, int y)
    {
        if (y == 0)
            return x;
        return returnNOD(y, x % y);
    }
}