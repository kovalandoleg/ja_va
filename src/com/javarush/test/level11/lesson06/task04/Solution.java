package com.javarush.test.level11.lesson06.task04;

/* Все мы работники
Написать четыре класса: Worker(сотрудник), Manager(управляющий), Chief(директор) и  Secretary(секретарь).
Унаследовать управляющего, директора и секретаря от сотрудника.
*/

public class Solution
{
    public class Manager {}

    public class Chief {}

    public class Worker {}

    public class Secretary {}
}
