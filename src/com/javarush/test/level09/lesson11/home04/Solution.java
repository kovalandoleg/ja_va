package com.javarush.test.level09.lesson11.home04;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/** Конвертер дат
Ввести с клавиатуры дату в формате «08/18/2013»
Вывести на экран эту дату в виде «AUG 18, 2013».
Воспользоваться объектом Date и SimpleDateFormat.
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        SimpleDateFormat inputformat = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat outputformatMonth = new SimpleDateFormat("MMM ");
        SimpleDateFormat outputformatElse  = new SimpleDateFormat("dd, yyyy");

        try
        {
            Date date = inputformat.parse(reader.readLine());
            System.out.println(outputformatMonth.format(date).toUpperCase() + outputformatElse.format(date));
        }
        catch (ParseException e)
        {
            System.out.println(e.getMessage());
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
