package com.javarush.test.level15.lesson12.home09;

import java.io.*;
import java.util.ArrayList;

/** Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14

 * or try javarush.ru/alpha/index.html?lvl=15&??view&&&name=Aobjmigo&obj=3.14&name=&obj=djsdcd&&?oobj=3.0
 * obj.javarush.ru/?op=obj&&obj=3&obj=3.3.3&values=5&object=this&obj=&oobj=7.7
 *
 * 1 После? все знаки? воспринимаются как разделители &
 2 Надо пробовать преобразовывать к Double все строки, даже если там нет "."

 Подсказки:
 1 Убрать двойные разделители
 2 Через try парсить число
 3 Использовать сплиты строк, чтобы получить массив строк. удобно обрабатывать параметры
 4 Не забывать про пустые имена параметров и значения
*/

public class Solution {
    public static void main(String[] args) {
        //add your code here
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}
